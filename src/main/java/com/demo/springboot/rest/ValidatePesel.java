package com.demo.springboot.rest;

import java.util.ArrayList;

public class ValidatePesel {

    public boolean checkSum(String pesel){
        ArrayList<Integer> peselArr = stringToArray(pesel);
        boolean state;
        if(peselArr.size() == 11) {
            int sum = 1 * peselArr.get(0) +
                    3 * peselArr.get(1) +
                    7 * peselArr.get(2) +
                    9 * peselArr.get(3) +
                    1 * peselArr.get(4) +
                    3 * peselArr.get(5) +
                    7 * peselArr.get(6) +
                    9 * peselArr.get(7) +
                    1 * peselArr.get(8) +
                    3 * peselArr.get(9);
            sum %= 10;
            sum = 10 - sum;
            sum %= 10;
            if( sum == peselArr.get(10)){
                state = true;
            }
            else{
                state = false;
            }
        } else {
            state = false;
        }

        return state;
    }
    public ArrayList<Integer> stringToArray(String pesel){
        ArrayList<Integer> peselArray = new ArrayList<Integer>();
        for(int i = 0; i<pesel.length(); i++){
            int temp = Integer.parseInt(pesel.substring(i,i+1));
            peselArray.add(temp);
        }
        return peselArray;
    }
}
